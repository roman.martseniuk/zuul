package com.epam.edp.foo.service;

/**
 * @author Pavlo_Yemelianov
 */
public interface AsyncService {
    void postMessage(String body);
}
